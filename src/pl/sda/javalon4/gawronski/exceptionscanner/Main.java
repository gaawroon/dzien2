package pl.sda.javalon4.gawronski.exceptionscanner;

import java.lang.reflect.Executable;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        // TARCZA ANTYKRYZYSOWA
        // Podaj ile srednio Twoja frima zarabiala na miesiac przed COVID
        // WYplacimy Ci 1000 zl + 20% srednich zarobkow
        // Jezeli podasz zla srednia, to przyjmujemy 0

        Scanner scanner = new Scanner(System.in);
        int x;
        try {
            x = scanner.nextInt();
        } catch(Exception e) {
            x = 0;
        }

        System.out.println(1000 + 0.2 * x);
    }
}
