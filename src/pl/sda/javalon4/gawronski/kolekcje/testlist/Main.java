package pl.sda.javalon4.gawronski.kolekcje.testlist;

import java.util.*;

public class Main {

    public static void main(String[] args) {

        List<String> mojaLista = new ArrayList<>(); // new LinkedList<>();

        List<List<String>> wielowymiarowaLista;

        System.out.println(mojaLista.size());

        mojaLista.add("Basia");
        mojaLista.add("Ania");
        mojaLista.add("Celina");

        print(mojaLista);

        mojaLista.add(1, "Daniela");

        for(int i=0; i<mojaLista.size(); i++) {
            System.out.println(mojaLista.get(i));
        }

        // porzadek odwrotny do leksykograficznego
        mojaLista.sort(Comparator.reverseOrder());
        print(mojaLista);
    }

    private static void print(List<String> mojaLista) {
        for(String s : mojaLista) {
            System.out.println(s);
        }
    }

}
