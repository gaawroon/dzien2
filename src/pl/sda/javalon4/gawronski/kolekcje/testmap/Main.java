package pl.sda.javalon4.gawronski.kolekcje.testmap;

import java.util.HashMap;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        // mapa ID_USERA -> EMAIL
        Map<Integer, String> mojaMapa = new HashMap<>();

        mojaMapa.put(1, "email@sda.pl");
        mojaMapa.put(4, "spam@gmail.com");

        System.out.println(mojaMapa.get(4));
        System.out.println(mojaMapa.get(1000));

        mojaMapa.put(1, "nowa wartosc");
        System.out.println(mojaMapa.get(1));

        System.out.println("***");

        for(Integer i : mojaMapa.keySet()) {
            System.out.println(i);
        }

        System.out.println("***");

        for(String s : mojaMapa.values()) {
            System.out.println(s);
        }

        System.out.println("***");

        for(Map.Entry<Integer, String> e : mojaMapa.entrySet()) {
            System.out.println("Dla klucza: " + e.getKey() + " wartosc to " + e.getValue());
        }

        System.out.println(mojaMapa.size());
        mojaMapa.putIfAbsent(4, "putifabsent@gmail.com");
        mojaMapa.putIfAbsent(8592, "putifabsent_8592@gmail.com");
        System.out.println(mojaMapa.size());
    }
}
