package pl.sda.javalon4.gawronski.kolekcje.testqueue;

import java.util.LinkedList;
import java.util.Queue;

public class Main {
    public static void main(String[] args) {
        // kolejka FIFO - first in, first out
        Queue<String> queue = new LinkedList<>();
        queue.add("1");
        queue.add("2");

        String x = queue.peek();
        System.out.println(x);
        queue.poll();

        queue.add("3");

        x = queue.poll();
        System.out.println(x);

        queue.add("4");
        queue.add("5");

        while(!queue.isEmpty()) {
            System.out.println(queue.poll());
        }

        System.out.println("Size = " + queue.size());
    }
}
