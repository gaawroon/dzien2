package pl.sda.javalon4.gawronski.kolekcje.testqueue;

import java.util.ArrayDeque;
import java.util.Deque;

public class MainLifo {

    public static void main(String[] args) {
        // LIFO - last in, first out
        Deque<String> mojaKolejka = new ArrayDeque<>();

        mojaKolejka.add("abc");
        mojaKolejka.add("def");

        System.out.println(mojaKolejka.pollLast());

        mojaKolejka.add("ghi");
        mojaKolejka.add("jkl");

        while(!mojaKolejka.isEmpty()) {
            System.out.println(mojaKolejka.pollLast());
        }
    }
}
