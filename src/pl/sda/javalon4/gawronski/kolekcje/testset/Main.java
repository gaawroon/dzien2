package pl.sda.javalon4.gawronski.kolekcje.testset;

import java.util.HashSet;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> mojZbior = new HashSet<>();
        mojZbior.add("xyz");
        mojZbior.add("abc");
        mojZbior.add("ghi");

        printSet(mojZbior);

        mojZbior.add("abc");
        printSet(mojZbior);

        mojZbior.add("ffff");
        printSet(mojZbior);
    }

    private static void printSet(Set<String> set) {
        System.out.println(set.size());
        for(String s : set ) {
            System.out.println("Element zbioru = " + s);
        }
    }
}
