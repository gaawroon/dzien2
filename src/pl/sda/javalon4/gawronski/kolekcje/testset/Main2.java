package pl.sda.javalon4.gawronski.kolekcje.testset;

import java.util.HashSet;
import java.util.Set;

public class Main2 {
    public static void main(String[] args) {
        Set<Integer> mojSet = new HashSet<>();
        mojSet.add(4);
        mojSet.add(194);
        mojSet.add(-1230);
        mojSet.add(1321);

        printSet(mojSet);
    }

    private static void printSet(Set<Integer> set) {
        System.out.println(set.size());
        for(Integer s : set ) {
            System.out.println("Element zbioru = " + s);
        }
    }
}
