package pl.sda.javalon4.gawronski.testenumow;

public class Main {
    public static void main(String[] args) {
        Pizza mojaPizza = new Pizza();
        mojaPizza.setType(PizzaType.MARGHERITA);
        mojaPizza.setPizzaSize(Size.SMALL);
        mojaPizza.setDipSize(Size.LARGE);
        mojaPizza.sayHi();

        Pizza mojaDrugaPizza = new Pizza();
        mojaDrugaPizza.setType(PizzaType.HAWAJSKA);
        mojaDrugaPizza.sayHi();
    }
}
