package pl.sda.javalon4.gawronski.testenumow;

public class Pizza {
    private Size pizzaSize;
    private Size dipSize;
    private PizzaType type;

    public void sayHi() {
        System.out.println("Pizza o rozmiarze " + pizzaSize +
                " i typie " + type +
                " z sosem o rozmiarze " + dipSize);
        System.out.println("Pizza w skali 0-1 jest smaczna na: " + type.getHowTasty());
    }

     public PizzaType getType() {
        return type;
    }

    public void setType(PizzaType type) {
        this.type = type;
    }

    public Size getPizzaSize() {
        return pizzaSize;
    }

    public void setPizzaSize(Size pizzaSize) {
        this.pizzaSize = pizzaSize;
    }

    public Size getDipSize() {
        return dipSize;
    }

    public void setDipSize(Size dipSize) {
        this.dipSize = dipSize;
    }
}
