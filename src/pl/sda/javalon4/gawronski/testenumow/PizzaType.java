package pl.sda.javalon4.gawronski.testenumow;

public enum PizzaType {
    MARGHERITA(0.5f),
    CAPRICIOSA(0.9f),
    HAWAJSKA(0.1f);
    // PizzaType pizzaType = new PizzaType(0.5f); - gdyby to byla klasa, a nie enum

    private float howTasty;

    PizzaType(float howTasty) {
        this.howTasty = howTasty;
    }

    public float getHowTasty() {
        return howTasty;
    }
}
