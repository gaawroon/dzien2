package pl.sda.javalon4.gawronski.testenumow;

public enum Size {
    SMALL,
    MEDIUM,
    LARGE
}
