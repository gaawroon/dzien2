package pl.sda.javalon4.gawronski.testexceptions;

public class Main {
    public static void main(String[] args) {
        String x = null;

        System.out.println("tuz przed");
        try {
            System.out.println("W try, przed .length()");
            System.out.println(x.length());
            System.out.println("W try, po .length()");

            System.out.println("W try, przed dzieleniem");
            System.out.println(5/0);
            System.out.println("W try, po dzieleniu");
        } /* catch(NullPointerException exception) {
            System.out.println("Uwaga, polecial null pointer exception");
        } catch(ArithmeticException exception) {
            System.out.println("Uwaga, polecial arithmetic exception");
        }  catch(Exception e)  {
            zlapie wszystkie wyjatki
        } */
        catch(NullPointerException | ArithmeticException exception) {
            System.out.println("Polecial albo NPE, AE");
            exception.printStackTrace();
        }


        System.out.println("tuz po");

        System.out.println(5 / 0);
        System.out.println("Koniec programu");
    }
}
