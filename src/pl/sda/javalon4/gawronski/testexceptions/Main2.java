package pl.sda.javalon4.gawronski.testexceptions;

public class Main2 {

    public static void main(String[] args) {
        // otwieranie pliku
        // petla x 100:
        //          obliczanie duzej wartosci
        //          zapis duzej wartosci do pliku
        // zamkniecie pliku     -> do bloku finally
        try {
            System.out.println("Przed");
            int i = 5 / 3;      // int i = 5 / 0;
            System.out.println("Po");
        }
        catch(Exception exception) {
            System.out.println("Obslugujemy wyjatek");
        } finally {
            System.out.println("Kod wykonuje sie niezaleznie od wyjatkow w bloku try");
        }
    }

}
