package pl.sda.javalon4.gawronski.wlasnyexception;

public class Main {

    public static void main(String[] args) {
        Person p1 = new Person();
        p1.setName("A");

        Person p2 = new Person();
        p2.setName("B");

        Person p3 = new Person();
        p3.setName("C");

        p1.setPartner(p2);
        p2.setPartner(p3);
        p3.setPartner(p2);
        try {
            marry(p1);
        } catch(PartnerNotInterestedInMarriageException exception) {
            System.out.println("Nie udalo sie wziac slubu - partner nie jest zainteresowanym slubem");
            System.out.println("Zeby wziac slub, musisz przekonac: " + exception.getPerson().getName());
        }

        // marry musi byc w try{}, bo rzucany wyjatek jest typu checked
        try {
            marry(p2);
        } catch (PartnerNotInterestedInMarriageException e) {
            e.printStackTrace();
        }
    }

    private static int marry(Person p1) throws PartnerNotInterestedInMarriageException {
        // sygnatura metody musi jawnie definiowac rzucany wyjatek, gdyz rzucany wyjatek jest typu checked
        Person p2 = p1.getPartner();
        if(p2.getPartner() != p1) {
            System.out.println("Sorry ale nie mozecie wziac slubu");
            throw new PartnerNotInterestedInMarriageException(p2);
        }

        System.out.println("Logika wziecia slubu");
        return 82492;     // ID slubu, np. nr dokumentu zawarcia slubu
    }

}
