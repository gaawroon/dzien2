package pl.sda.javalon4.gawronski.wlasnyexception;


// TRZY PODSTAWOWE BAZY KLASOWE DLA WYJATKOW:
// 1) RuntimeException              // unchecked exception
// 2) Exception                     // checked exception
// 3) Error                         // naprawde powazne problemy, np. z JVM
public class PartnerNotInterestedInMarriageException extends Exception {

    private Person person;

    public PartnerNotInterestedInMarriageException(Person person) {
        this.person = person;
    }

    public Person getPerson() {
        return person;
    }
}
