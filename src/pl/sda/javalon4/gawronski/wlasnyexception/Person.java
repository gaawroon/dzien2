package pl.sda.javalon4.gawronski.wlasnyexception;

public class Person {
    private String name;
    private Person partner;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Person getPartner() {
        return partner;
    }

    public void setPartner(Person partner) {
        this.partner = partner;
    }
}
